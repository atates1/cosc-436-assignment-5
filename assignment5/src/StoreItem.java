/*
 * author @aniesetates
 */
public class StoreItem {
private String itemCode;
private String itemDescription;
private double itemPrice;

	public StoreItem(String code, String descr, double price) {
		// TODO Auto-generated constructor stub
		itemCode = code;
		itemDescription = descr;
		itemPrice = price; 
	}

	public String getItemCode() {
		// TODO Auto-generated method stub
		return this.itemCode;
	}
	public void setItemCode(String c) {
		itemCode = c;
	}

	public double getItemPrice() {
		// TODO Auto-generated method stub
		return this.itemPrice;
	}
	public void setItemPrice(double d) {
		itemPrice = d;
	}

	public String getItemDesc() {
		// TODO Auto-generated method stub
		return this.itemDescription;
	}
	public void setItemDescription(String dc) {
		itemDescription = dc;
	}

}
