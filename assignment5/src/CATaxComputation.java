/*
 * author @aniesetates
 */



public class CATaxComputation implements TaxComputation {

	private String statecode = "CA";

@Override
	public double computeTax (PurchasedItems items, ReceiptDate date){
		if (taxHoliday(date))
			return 0.0;
			return items.getTotalCost() * 0.0725;
}

@Override
public boolean taxHoliday (ReceiptDate date){
		if (date.getDate().equals("13/08/2016") || date.getDate().equals("14/08/2016"))
				return true;
			return false;
		}
	public String getStateCode(){
		return statecode;
	}
}