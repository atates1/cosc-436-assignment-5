/*
 * author @aniesetates
 */
public class HolidayGreeting implements SecondaryHeader, AddOn {
	public boolean applies(PurchasedItems items) {
		return true;
	}
	public String getLines() {
		return "Happy Holidays from Best Buy";
	}
}
