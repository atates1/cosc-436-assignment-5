/*
 * author @aniesetates
 */
public class Coupon100Get10Percent implements Coupon, AddOn {
	
	
	public boolean applies(PurchasedItems items) {
		if(items.getTotalCost() > 100)
			return true;
		return false;
	}
	public String getLines() {
		return "Coupon applied, 10% off on next purchase";
	}
}
