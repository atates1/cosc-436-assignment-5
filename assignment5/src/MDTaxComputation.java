/*
 * author @aniesetates
 */

public class MDTaxComputation implements TaxComputation {

	private String statecode = "MD";

@Override
	public double computeTax (PurchasedItems items, ReceiptDate date){
		if (taxHoliday(date))
			return 0.0;
			return items.getTotalCost() * 0.0625;
	}

@Override
public boolean taxHoliday (ReceiptDate date){
		if (date.getDate().equals("13/08/2016") || date.getDate().equals("14/08/2016"))
			return true;
			return false;
	}
	public String getStateCode()
		{
		return statecode;
		}

public double getSalesTax() {
	// TODO Auto-generated method stub
	return 0.06;
}


}
