/*
 * author @aniesetates
 */



public interface TaxComputation {
   

public abstract double computeTax(PurchasedItems items, ReceiptDate date);

abstract boolean taxHoliday(ReceiptDate date);
  
}