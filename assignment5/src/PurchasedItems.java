/*
 * author @aniesetates
 */
import java.util.ArrayList;



public class PurchasedItems {
	
	private ArrayList<StoreItem> items;
	
	public PurchasedItems() {
		items = new ArrayList();
	}
	
	public boolean containsItem(String itemCode) {
		for(int i = 0; i<items.size(); i++) {
			if (items.get(i).getItemCode().equals(itemCode))
				return true;
		}
		return false;
	}
	
	public double getTotalCost() {
		double c = 0;
		for (int i =0; i<items.size(); i++) {
			c=c+items.get(i).getItemPrice();
		}
		return c;
	}
	
	public void addItem(StoreItem item) {
		items.add(item);
	}

	public int size() {
		// TODO Auto-generated method stub
		return items.size();
	}

	public StoreItem get(int i) {
		// TODO Auto-generated method stub
		return items.get(i);
	}

	public boolean hasNext() {
		// TODO Auto-generated method stub
		return false;
	}

	public Object next() {
		// TODO Auto-generated method stub
		return null;
	}
}
